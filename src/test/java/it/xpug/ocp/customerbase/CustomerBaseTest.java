package it.xpug.ocp.customerbase;

import static java.util.Arrays.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.*;

import it.xpug.ocp.customerbase.finds.FindByFirstAndLastName;
import it.xpug.ocp.customerbase.finds.FindByGreaterCredit;
import it.xpug.ocp.customerbase.finds.FindByLastName;
import org.junit.*;


public class CustomerBaseTest {

	private final Customer alice = new Customer("Alice", "Rossi", 10000);
	private final Customer bob = new Customer("Bob", "Rossi", 20000);
	private final Customer charlie = new Customer("Charlie", "Bianchi", 30000);

	private List<Customer> customers = new ArrayList<Customer>();
	private final CustomerBase customerBase = new CustomerBase();
	private final FindByFirstAndLastName findByFirstAndLastName = new FindByFirstAndLastName();
	private final FindByGreaterCredit findByGreaterCredit = new FindByGreaterCredit();
	private final FindByLastName findByLastName = new FindByLastName();

	@Before
	public void setUp() {
		customerBase.setCustomers(new ArrayList<Customer>());
		customerBase.add(alice);
		customerBase.add(bob);
		customerBase.add(charlie);
		customers = customerBase.getCustomers();
	}

	@Test
	public void findCustomersByLastName() {
		List<Customer> found = findByLastName.find(customers, alice);
		assertThat(found, is(asList(alice, bob)));
	}

	@Test
	public void findCustomersByFirstAndLastName() {
		List<Customer> found = findByFirstAndLastName.find(customers, alice);
		assertThat(found, is(asList(alice)));
	}

	@Test
	public void findCustomersWithCreditGreaterThan() {
		List<Customer> found = findByGreaterCredit.find(customers, bob);
		assertThat(found, is(asList(charlie)));
	}
}
