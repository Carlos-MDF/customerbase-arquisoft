package it.xpug.ocp.customerbase;
import java.util.*;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class CustomerBase {

	public void add(Customer customer) {
		customers.add(customer);
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	private List<Customer> customers;
}
