package it.xpug.ocp.customerbase.finds;

import it.xpug.ocp.customerbase.Customer;

import java.util.List;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public interface Finder {

    List<Customer> find(final List<Customer> customers, final Customer customerToSearch);
}
