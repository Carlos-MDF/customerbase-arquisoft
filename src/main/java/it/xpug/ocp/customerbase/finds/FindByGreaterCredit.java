package it.xpug.ocp.customerbase.finds;

import it.xpug.ocp.customerbase.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Carlos Eduardo Terceros Rojas
 */
public class FindByGreaterCredit implements Finder {

    @Override
    public List<Customer> find(final List<Customer> customers, final Customer customerToSearch) {

        List<Customer> result = new ArrayList<Customer>();
        for (Customer customer : customers) {
            if (customer.credit() > customerToSearch.credit()) {
                result.add(customer);
            }
        }
        return result;
    }
}
